# Monorepo pour tests React, Express et e2e

## Contexte

On veut créer un dépôt contenant des applications backend (Express) et frontend (React).

Ce sera donc un _monorepo_ - terminologie utilisée pour désigner un dépôt contenant plusieurs applications ou bibliothèques.

Des exemples connus de bibliothèques utilisant un _monorepo_ sont React, React Native, Angular, etc.

Notre monorepo sera nommé `express-react-testing-monorepo`.

Voici sa structure globale :

```
express-react-testing-monorepo/
├── package.json          # package.json global du npm Workspaces
├── backend/
│   ├── package.json      # package.json du backend Express
│   ├── package-lock.json
│   ├── src/              # Sources TypeScript du backend
│   ├── tsconfig.json
│   └── ...               # Reste masqué pour concision
└── frontend/
    ├── package.json      # package.json du frontend React
    ├── package-lock.json
    ├── src/              # Sources TypeScript du frontend
    ├── tsconfig.json
    └── ...               # Reste masqué pour concision

```

## Initialisation

> Seulement si vous souhaitiez refaire toute la mise en place. Tout n'est pas détaillé ici : on a du effectuer un certain nombre d'étapes de configuration des outils.

### Création du "monorepo"

```
mkdir express-react-testing-monorepo
cd express-react-testing-monorepo
npm init -y
git init
```

Un fichier `package.json` et un dossier `.git` sont créés à la racine.

Dans `package.json`, ajouter cette ligne :

```
  "workspaces": ["backend", "frontend"]
```

### Création du dossier `backend`

Toujours dans `express-react-testing-monorepo` :

```
mkdir backend
cd backend
npm init -y
cd ..
```

Un fichier `package.json` est créé sous `backend`. La dernière commande permet de revenir à la racine du monorepo.

### Création du dossier `frontend`.

Toujours dans `express-react-testing-monorepo`. On va utiliser [Vite](https://vitejs.dev/) pour initialiser ce dossier.

```
npm create vite frontend
```

- On va nous demander de choisir un _framework_ &rarr; Choisir _React_.
- Puis on va nous demander de choisir une _variante_ pour configurer les outils de build &rarr; Choisir _TypeScript + SWC_

Installation des dépendances :

```
npm i -D @testing-library/jest-dom @testing-library/react @types/jest jsdom vitest @vitest/coverage-v8 msw
```

- `@testing-library/jest-dom` et `@testing-library/react` &rarr; composants de [React Testing Library](https://testing-library.com/docs/react-testing-library/intro)
- `vitest` &rarr; _test runner_ offrant une API similaire à Jest
- `@types/jest` &rarr; types pour Jest, largement interchangeables avec ceux de Vitest (définitions de `describe`, `it`, etc.)
- `jsdom` &rarr; simule un environnement browser pour Jest / Vitest
- `@vitest/coverage-v8` &rarr; outil de _code coverage_, montrant des "métriques" sur la proportion du code couvert par les tests
- `msw` permet de "mocker" un serveur (API REST ou GraphQL) afin d'effectuer des tests impliquant des requêtes HTTP, sans lancer un serveur

Création de fichiers :

- `vitest.config.ts` &rarr; configuration de l'outil Vitest
- `src/_setupTests.ts` &rarr; import des "matchers" Jest pour les tests d'application frontend